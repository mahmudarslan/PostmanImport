﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace mahmud.Postman.Controllers
{
    [Route("postman")]
    public class PostmanController : Controller
    {
        private readonly IApiDescriptionGroupCollectionProvider _apiExplorer;
        private readonly IHostingEnvironment _env;

        public PostmanController(IApiDescriptionGroupCollectionProvider apiExplorer, IHostingEnvironment environment)
        {
            _apiExplorer = apiExplorer;
            _env = environment;
        }

        private static string FormatResourceName(Assembly assembly, string resourceName)
        {
            return assembly.GetName().Name + "." + resourceName.Replace(" ", "_")
                                                               .Replace("\\", ".")
                                                               .Replace("/", ".");
        }

        public static string GetEmbeddedResource(string resourceName, Assembly assembly)
        {
            resourceName = FormatResourceName(assembly, resourceName);
            using (Stream resourceStream = assembly.GetManifestResourceStream(resourceName))
            {
                if (resourceStream == null)
                    return null;

                using (StreamReader reader = new StreamReader(resourceStream))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        public string GetResource(string rootPath, string environmentName)
        {
            using (StreamReader resourceStream = new StreamReader($@"{rootPath}\appsettings.{environmentName}.json"))
            {
                if (resourceStream == null)
                    return null;

                return resourceStream.ReadToEnd();
            }
        }





       
        [HttpGet]
        public object Postman()
        {
            var jsonEnviroment = GetEmbeddedResource("appsettings.json", this.GetType().Assembly);
            var postman = JsonConvert.DeserializeObject<JObject>(jsonEnviroment)["Postman"];

            var json = GetResource(_env.ContentRootPath, _env.EnvironmentName);
            var postmanEnvironment = JsonConvert.DeserializeObject<JObject>(json)["PostmanEnvironment"];

            var forAddEnviroment = new List<string>();

            postman["info"] = Info(postman["info"], postmanEnvironment);
            postman["item"] = Item(postman["item"], ref forAddEnviroment);
            postman["auth"] = Auth(postman["auth"]);
            postman["event"] = Event(postman["event"], postmanEnvironment, forAddEnviroment);
            return postman;
        }

        JToken Info(JToken data, JToken pEnvironment)
        {
            data["_postman_id"] = Guid.NewGuid().ToString();
            data["name"] = $"{_env.ApplicationName} {_env.EnvironmentName} {DateTime.Now.ToShortDateString()}";
            data["description"] = pEnvironment["ProjectDescription"];
            return data;
        }

        JToken Item(JToken Folders, ref List<string> forAddEnviroment)
        {
            var tempFolder = Folders.First.DeepClone();
            Folders.First.Remove();
            var Conttrolers = _apiExplorer.ApiDescriptionGroups.Items[0].Items.GroupBy(x => x.ActionDescriptor.RouteValues.LastOrDefault()).ToList();
            foreach (var actions in Conttrolers)
            {
                var folder = tempFolder.DeepClone();

                #region Folder

                folder["name"] = actions.FirstOrDefault().ActionDescriptor.RouteValues.LastOrDefault().Value;

                #region Folder Item

                foreach (var action in actions)
                {
                    var folderItem = tempFolder["item"].First.DeepClone();
                    var folderItemRequest = folderItem["request"].DeepClone();

                    folderItem["name"] = action.RelativePath;
                    #region Folder Item Request
                    folderItemRequest["method"] = action.HttpMethod;
                    folderItemRequest["header"] = new JArray();
                    ((JValue)folderItemRequest["body"].SelectToken("mode")).Value = "raw";
                    ((JValue)folderItemRequest["body"].SelectToken("raw")).Value = "";
                    ((JValue)folderItemRequest["url"].SelectToken("raw")).Value = $"{{ApiPath}}/{action.RelativePath}";
                    ((JArray)folderItemRequest["url"].SelectToken("host")).Add("{{ApiPath}}");
                    ((JArray)folderItemRequest["url"].SelectToken("path")).Add(action.RelativePath);
                    foreach (var paramerter in action.ParameterDescriptions)
                    {
                        if (paramerter.Source.Id != "Body" && paramerter.Source.Id != "Path")
                        {
                            if (forAddEnviroment.FirstOrDefault(t => t.Contains(paramerter.Name)) == null)
                            {
                                forAddEnviroment.Add(paramerter.Name);
                            }
                            ((JArray)folderItemRequest["url"].SelectToken("query")).Add(JToken.Parse($@"{{""key"": ""{paramerter.Name}"",""value"": ""{{{{{paramerter.Name}}}}}""}}"));
                        }
                    }
                    #endregion
                    folderItem["request"] = folderItemRequest;
                    folderItem["response"] = new JArray();

                    ((JArray)folder["item"]).Add(folderItem.DeepClone());
                }

                #endregion

                folder["description"] = $"Folder for {actions.FirstOrDefault().ActionDescriptor.DisplayName}";

                #endregion

                ((JArray)folder["item"]).First.Remove();
                ((JArray)Folders).Add(folder.DeepClone());
            }

            return Folders;
        }

        JToken Auth(JToken data)
        {
            return data;
        }

        JToken Event(JToken data, JToken pEnvironment, List<string> forAddEnviroment)
        {
            var loginConfig = (JArray)(data[0]["script"].SelectToken("exec")).DeepClone();
            ((JArray)(data[0]["script"].SelectToken("exec"))).Clear();

            //Main Postman Auth Config 
            ((JArray)(data[0]["script"].SelectToken("exec"))).Add($@"pm.environment.set(""{"ApiPath"}"",pm.environment.get(""{"ApiPath"}"") ? pm.environment.get(""{"ApiPath"}"") : ""{pEnvironment["ApiPath"]}"");");
            ((JArray)(data[0]["script"].SelectToken("exec"))).Add($@"pm.environment.set(""{"AccessTokenURL"}"",pm.environment.get(""{"AccessTokenURL"}"") ? pm.environment.get(""{"AccessTokenURL"}"") : ""{pEnvironment["AccessTokenURL"]}"");");
            ((JArray)(data[0]["script"].SelectToken("exec"))).Add($@"pm.environment.set(""{"ClientID"}"",pm.environment.get(""{"ClientID"}"") ? pm.environment.get(""{"ClientID"}"") : ""{pEnvironment["ClientID"]}"");");
            ((JArray)(data[0]["script"].SelectToken("exec"))).Add($@"pm.environment.set(""{"ClientSecret"}"",pm.environment.get(""{"ClientSecret"}"") ? pm.environment.get(""{"ClientSecret"}"") : ""{pEnvironment["ClientSecret"]}"");");
            ((JArray)(data[0]["script"].SelectToken("exec"))).Add($@"pm.environment.set(""{"Scope"}"",pm.environment.get(""{"Scope"}"") ? pm.environment.get(""{"Scope"}"") : ""{pEnvironment["Scope"]}"");");

            //Query Enviroment Data 
            foreach (var item in forAddEnviroment)
            {
                ((JArray)(data[0]["script"].SelectToken("exec"))).Add($@"pm.environment.set(""{item}"",pm.environment.get(""{item}"") ? pm.environment.get(""{item}"") : '');");
            }

            //Auto Update Auth Info Code
            foreach (var item in loginConfig)
            {
                ((JArray)(data[0]["script"].SelectToken("exec"))).Add(((JValue)item).Value);
                ((JArray)(data[0]["script"].SelectToken("exec")))[((JArray)(data[0]["script"].SelectToken("exec"))).Count - 1] = item;
            }
            return data;
        }
    }
}
